# README
## Content
```
.
|-docker
|  |-- web
|  
|-infra
    |---terraform
        |-----account-infra-stack
              +------- test
        |-----remote-state
              +------- test
        |-----web-stack
              +------- test
|-public
```

`docker/web` contains the actual page and Dockerfile

`infra/terraform/remote-state` This is the terraform remote state, used by the other stacks. S3 bucket
with a DynamoDB TF lob table. This terraform folder utilizes `terraform workspaces`

`infra/terraform/account-infra-stack/test` Account level stuff, VPC subnets etc. 

`infra/terraform/web-stack/test` Terraform code for the actual web stack, AWS RDS, Fargate + LB
 
# Deploy
Before deploying make sure you have your AWSCLI tokens etc setup.  
`aws sts get-caller-identity`

## Setup terraform remote-state
1. cd `infra/terraform/remote-state`
2. `terraform workspace new test`
3. `terraform init`
4. `terraform apply -auto-approve`
5. Copy bucket name `terraform_state_bucket_id` from the stack outputs and use it in `infra/terraform/web-stack/test/backend.tf` and `infra/terraform/account-infra-stack/test/backend.tf`

## Deploy account infra
1. cd `infra/terraform/account-infra-stack/test`
2. `terraform init`
3. `terraform plan`
4. `terraform apply`


## deploy app
1. cd `infra/terraform/web-stack/test`
2. `terraform init`
3. `terraform plan`
4. `terraform apply`
5. Copy `LB_DNS_NAME` from the output


## build the image, tag and push
1. `docker build -t php_app . -f docker/web/Dockerfile`
2. `docker build docker tag php_app:latest <account nr>.dkr.ecr.eu-west-1.amazonaws.com/php_app:latest`
3. `aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin <account nr>.dkr.ecr.eu-west-1.amazonaws.com`
4. `docker push <account nr>.dkr.ecr.eu-west-1.amazonaws.com/php_app:latest`
5. Navigate to `LB_DNS_NAME` from the `deploy app` step 5.

