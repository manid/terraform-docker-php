data "aws_caller_identity" "current" {}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "${data.aws_caller_identity.current.account_id}-tfstate"
  acl    = "private"

  versioning {
    enabled = true
  }

  # Protect buckets
  force_destroy = false
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "terraform_state_lock" {
  name           = "tfstate-lock"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

output "terraform_state_bucket_id" {
  value = aws_s3_bucket.terraform_state.id
}

output "terraform_state_dynamodb_lock_table" {
  value = aws_dynamodb_table.terraform_state_lock.name
}

