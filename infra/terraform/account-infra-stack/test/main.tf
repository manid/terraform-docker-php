module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "vpc"
  cidr = "10.0.0.0/16"

  single_nat_gateway = true

  azs                   = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  private_subnets       = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets        = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  public_subnet_suffix  = "public"
  private_subnet_suffix = "private"
  database_subnets      = ["10.0.21.0/24", "10.0.22.0/24"]

  enable_nat_gateway   = true
  enable_vpn_gateway   = true
  enable_dns_hostnames = true

}

output "database_subnet_group_name" {
  value = module.vpc.database_subnet_group_name
}

output "private_subnets" {
  value = module.vpc.private_subnets
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

