terraform {
  backend "s3" {
    region         = "eu-west-1"
    encrypt        = "true"
    bucket         = "493201068155-tfstate"
    key            = "account-infra-stack/terraform.tfstate"
    dynamodb_table = "tfstate-lock"
  }
}

