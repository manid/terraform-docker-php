data "aws_iam_policy" "AmazonECSTaskExecutionRolePolicy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role" "ECSTaskExecutionRole" {
  name               = "ecs-taskexec"
  assume_role_policy = data.aws_iam_policy_document.AssumeRolePolicyDocument_ecs_exec.json
}

resource "aws_iam_role_policy_attachment" "AmazonECSTaskExec_to_execrole" {
  policy_arn = data.aws_iam_policy.AmazonECSTaskExecutionRolePolicy.arn
  role       = aws_iam_role.ECSTaskExecutionRole.id
}

data "aws_iam_policy_document" "AssumeRolePolicyDocument_ecs_exec" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}
