resource "aws_security_group" "sg_fargate" {
  name_prefix = "tf-far"
  description = "/security_group_fargate.tf"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "SG_FARGATE"
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}


