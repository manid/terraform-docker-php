resource "aws_iam_role" "TaskRole" {
  name               = "ecs-task-service"
  assume_role_policy = data.aws_iam_policy_document.AssumeRolePolicyDocument_ecs_tasks.json
}

data "aws_iam_policy_document" "AssumeRolePolicyDocument_ecs_tasks" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}
