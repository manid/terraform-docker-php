resource "aws_security_group" "sg_lb" {
  name_prefix = "tf-lb"
  description = "/security_groups_lb.tf"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "SG_LG"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

