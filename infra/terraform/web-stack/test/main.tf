## Create DB and table `hello`
resource "aws_db_instance" "default" {
  identifier             = "default"
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  username               = "root"
  password               = "12345678"
  db_subnet_group_name   = "vpc"
  vpc_security_group_ids = [aws_security_group.sg_rds.id]
  publicly_accessible    = false
  skip_final_snapshot    = true
  name                   = "hello"
}
