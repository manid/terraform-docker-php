resource "aws_security_group" "sg_rds" {
  name_prefix = "tf-rds"
  description = "/security_groups_rds.tf"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "SG_RDS"
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


