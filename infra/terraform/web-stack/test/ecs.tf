
resource "aws_ecr_repository" "php" {
  name = "php_app"
}

resource "aws_ecs_cluster" "php_app" {
  name = "php_app"
}

resource "aws_ecs_service" "php_container" {
  name            = "php_app"
  cluster         = aws_ecs_cluster.php_app.id
  task_definition = aws_ecs_task_definition.php_container.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets         = data.aws_subnets.private.ids
    security_groups = [aws_security_group.sg_fargate.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_tg.arn
    container_name   = "php_app"
    container_port   = 80
  }
}

resource "aws_ecs_task_definition" "php_container" {
  family       = "php_app"
  network_mode = "awsvpc"
  cpu          = 256
  memory       = 512

  task_role_arn      = aws_iam_role.TaskRole.arn
  execution_role_arn = aws_iam_role.ECSTaskExecutionRole.arn

  requires_compatibilities = [
    "FARGATE"
  ]
  container_definitions = jsonencode([
    {
      name      = "php_app"
      image     = "${data.aws_caller_identity.current.account_id}.dkr.ecr.eu-west-1.amazonaws.com/php_app"
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      environment = [
        {
          name : "MYSQL_PASSWORD"
          value : "12345678"
        },
        {
          name : "MYSQL_DATABASE"
          value : "hello"
        },
        {
          name : "MYSQL_HOST"
          value : aws_db_instance.default.endpoint
        },
        {
          name : "MYSQL_USERNAME"
          value : "root"
        }
      ]
    }
  ])
}

