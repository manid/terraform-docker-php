
resource "aws_lb" "alb" {
  name_prefix        = "tf-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg_lb.id]
  subnets            = data.aws_subnets.public.ids
  tags = {
    description = "Load balancer"
  }
}


resource "aws_lb_target_group" "ecs_tg" {
  name        = "tf-ecs-tg"
  protocol    = "HTTP"
  port        = 80
  target_type = "ip"
  tags = {
    Description = "Fargate Target group"
  }

  vpc_id = data.aws_vpc.selected.id

  stickiness {
    enabled = false
    type    = "lb_cookie"
  }
}


resource "aws_lb_listener" "ecs_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_tg.arn
  }
}

output "LB_DNS_NAME" {
  value = aws_lb.alb.dns_name
}
